###################################################################
##                         User configuration                    ##
###################################################################

### Antibody
PATH=~/.local/bin:$PATH

if [ ! -x ~/.antidote ]; then
  echo "Installing Antidote plugin manager for Zsh..."
  git clone --depth=1 https://github.com/mattmc3/antidote.git ~/.antidote
else
  source ~/.antidote/antidote.zsh
  antidote load ~/.zsh_plugins
fi

# Source the .profile file here to avoid alias override from oh-my-zsh
[[ -f ~/.profile ]] && . ~/.profile

### Don't share history between Zsh instances, and ignore commands that start
### with a space.
# http://leetschau.github.io/remove-duplicate-zsh-history.html
# http://zsh.sourceforge.net/Doc/Release/Options.html
setopt NO_SHARE_HISTORY       # don't share command history data
setopt EXTENDED_HISTORY       # record timestamp of command in HISTFILE
setopt INC_APPEND_HISTORY     # add commands to HISTFILE in order of execution
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_BEEP

### Keybindings
bindkey \^U backward-kill-line

### HISTSIZE to keep within one session, SAVEHIST num lines to save.
SAVEHIST=14500
HISTSIZE=14500

### Don't kill bg jobs
# http://stackoverflow.com/questions/19302913
# http://zsh.sourceforge.net/Guide/zshguide02.html
setopt NO_HUP

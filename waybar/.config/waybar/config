{
    "layer": "bottom", // Waybar layer
    "height": 22, // Waybar height
    "modules-left": ["sway/workspaces", "sway/mode", "sway/window"],
    "modules-center": ["mpd", "custom/playerctl"],
    "modules-right": ["pulseaudio", "network", "battery", "backlight", "cpu", "memory", "clock", "tray"],
    // Modules configuration
    "sway/workspaces": {
         "disable-scroll": true,
         "all-outputs": true,
         "format": "{icon}",
         "format-icons": {
             "1": "1",
             "2": "2",
             "3": "3",
             "4": "4",
             "5": "5",
             "6": "6",
             "7": "7",
             "8": "8",
             "9": "9" ,
             "urgent": "",
             "focused": "",
             "default": ""
         }
     },
    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },
    "backlight": {
        "device": "intel_backlight",
        "format": "{percent}% {icon}",
        "format-icons": ["", ""]
    },
    "battery": {
        "states": {
            "good": 95,
            "warning": 30,
            "critical": 12
        },
        "format": "{capacity}% {icon}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": ["", "", "", "", ""]
    },
    "battery#bat2": {
        "bat": "BAT2"
    },
    "clock": {
        "format": "{:%a %d %b, %I:%M %p}",
        "on-click": "alacritty --class float -e sh -c 'cal -3 && tail -f /dev/null'"
    },
    "cpu": {
        "format": "{usage}% CPU "
    },
    "idle_inhibitor": {
    "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "memory": {
        "format": "{}% RAM ",
        "interval": 15
    },
    "mpd": {
        // "format": "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ",
        "format": "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ",
        "format-disconnected": "Disconnected ",
        "format-stopped": "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ",
        "unknown-tag": "N/A",
        "interval": 2,
        "consume-icons": {
            "on": " "
        },
        "random-icons": {
            "on": " "
        },
        "repeat-icons": {
            "on": " "
        },
        "single-icons": {
            "on": "1 "
        },
        "state-icons": {
            "paused": "",
            "playing": ""
        },
        // "max-length": 50,
        "tooltip-format": "MPD (connected)",
        "tooltip-format-disconnected": "MPD (disconnected)",
        "on-click": "alacritty --class float -e ncmpcpp"
    },
    "network": {
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
        "format-disconnected": "Disconnected ⚠",
        "interval": 10,
        "on-click": "alacritty --class float -e nmtui-connect"
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "pulseaudio": {
        "format": "{volume}% {icon}",
        "format-bluetooth": "{volume}% {icon}",
        "format-muted": "",
        "format-icons": {
            "headphones": "",
            "handsfree": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", ""]
        },
        "scroll-step": 1,
        "on-scroll-up": "pactl set-sink-volume '@DEFAULT_SINK@' +5%",
        "on-scroll-down": "pactl set-sink-volume '@DEFAULT_SINK@' -5%",
        "on-click": "pavucontrol || alacritty --class float -e pulsemixer"
    },
    "tray": {
        // "icon-size": 21,
        "spacing": 10
    },
    "custom/playerctl": {
        "format": "{} ",
        "exec": "$HOME/.config/waybar/modules/media-player-metadata.sh",
        "exec-if": "playerctl --ignore-player=mpd status 2>/dev/null | grep Playing",
        "interval": 2,
        "max-length": 40,
        "on-click": "playerctl play-pause",
        "on-click-right": "playerctl next",
        "on-scroll-up": "playerctl volume 0.05+",
        "on-scroll-down": "playerctl volume 0.05-"
    }
}

-- Maps
vim.keymap.set("n", "<Leader>cz", "<cmd>!yamlfix -c ~/.config/yamlfix/yamlfix.toml %<CR>")

local file_name = vim.api.nvim_buf_get_name(0)
if string.match(file_name, ".circleci/config.yml") then
  vim.keymap.set("n", "<Leader>r", "<cmd>!circleci --skip-update-check config validate<CR>", { buffer = true })
elseif string.match(file_name, ".github/**/*.yml") then
  vim.keymap.set("n", "<Leader>r", "<cmd>!actionlint<CR>", { buffer = true })
elseif string.match(file_name, "bitrise.yml") then
  vim.keymap.set("n", "<Leader>r", "<cmd>!bitrise validate && bitrise workflows<CR>", { buffer = true })
end

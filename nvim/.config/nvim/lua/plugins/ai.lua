return {
  {
    "jackMort/ChatGPT.nvim",
    lazy = true,
    dependencies = {
      "MunifTanjim/nui.nvim",
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope.nvim",
    },
    cmd = {
      "ChatGPT",
      "ChatGPTActAs",
      "ChatGPTEditWithInstructions",
      "ChatGPTRun",
    },
    opts = {
      model = "gpt-4o-mini",
      api_key_cmd = "bash " .. vim.fn.expand("$HOME") .. "/.credentials/openai.sh",
    },
  },
}

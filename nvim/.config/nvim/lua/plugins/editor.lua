return {
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    opts = {
      preset = "classic",
    },
  },

  {
    "kylechui/nvim-surround",
    version = "*",
    -- lazy = true,
    event = "VeryLazy",
    opts = {},
    dependencies = "ggandor/leap.nvim", -- Override leap maps
  },

  {
    "inkarkat/vim-ReplaceWithRegister",
    lazy = false,
    event = "InsertCharPre",
  },

  {
    "max397574/better-escape.nvim",
    lazy = true,
    event = "InsertCharPre",
    opts = {},
  },

  -- Text filtering and alignment
  {
    "godlygeek/tabular",
    lazy = true,
    cmd = "Tabularize",
  },

  -- Supertab
  -- Use <tab> for completion and snippets
  -- First disable default <tab> and <s-tab> behavior in LuaSnip

  {
    "L3MON4D3/LuaSnip",
    keys = function()
      return {}
    end,
    dependencies = {
      "rafamadriz/friendly-snippets",
    },
  },

  -- Set Super-tab in cmp
  {
    "saghen/blink.cmp",
    event = "InsertEnter",
    opts = function(_, opts)
      opts.keymap = opts.keymap or {}
      opts.keymap.preset = "enter"

      -- Helper to check for whitespace
      local has_words_before = function()
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        if col == 0 then
          return false
        end
        local text = vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]
        return not text:sub(col, col):match("%s")
      end

      opts.keymap["<Tab>"] = {
        "select_next", -- 1) Try to select the next menu item
        "snippet_forward", -- 2) Otherwise, jump snippet
        function(cmp) -- 3) Otherwise, if words exist, show the menu
          if has_words_before() then
            return cmp.show()
          end
          return false
        end,
        "fallback", -- 4) Otherwise, fall back to <Tab>
      }

      opts.keymap["<S-Tab>"] = {
        "select_prev",
        "snippet_backward",
        "fallback",
      }

      opts.keymap["<CR>"] = {
        "accept",
        "fallback",
      }
    end,
  },

  {
    "RRethy/nvim-treesitter-endwise",
    lazy = true,
    event = "BufRead",
    config = function()
      require("nvim-treesitter.configs").setup({
        endwise = {
          enable = true,
        },
      })
    end,
  },

  {
    "ThePrimeagen/refactoring.nvim",
    event = "BufReadPost",
    ft = { "typescript", "javascript", "lua", "c", "cpp", "go", "ruby", "python", "java" },
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
    },
    opts = {},
  },

  -- {
  --   -- REPL
  --   "michaelb/sniprun",
  --   lazy = true,
  --   build = "bash ./install.sh",
  -- },
}

-- Neovide
--
-- https://neovide.dev/faq.html

-- Keymaps
if vim.g.neovide then
  vim.keymap.set("n", "<D-s>", ":w<CR>") -- ⌘s Save
  vim.keymap.set("v", "<D-c>", '"+y') -- ⌘c Copy
  vim.keymap.set("n", "<D-v>", '"+P') -- ⌘v Paste normal mode
  vim.keymap.set("v", "<D-v>", '"+P') -- ⌘v Paste visual mode
  vim.keymap.set("c", "<D-v>", "<C-R>+") -- ⌘v Paste command mode
  vim.keymap.set("i", "<D-v>", '<ESC>l"+Pli') -- ⌘v Paste insert mode
end

vim.api.nvim_set_keymap("", "<D-v>", "+p<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("!", "<D-v>", "<C-R>+", { noremap = true, silent = true })
vim.api.nvim_set_keymap("t", "<D-v>", "<C-R>+", { noremap = true, silent = true })
vim.api.nvim_set_keymap("v", "<D-v>", "<C-R>+", { noremap = true, silent = true })

-- Zoom
vim.g.neovide_scale_factor = 1.2
local change_scale_factor = function(delta)
  vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
end
vim.keymap.set("n", "<D-=>", function()
  change_scale_factor(1.1)
end)
vim.keymap.set("n", "<D-->", function()
  change_scale_factor(1 / 1.1)
end)

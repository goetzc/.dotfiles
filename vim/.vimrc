""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" General settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Show line numbers and set it relative
set number
set relativenumber

" Split new windows below and to the right position
set splitbelow
set splitright

" Syntax highlighting
syntax on

" Search
set ignorecase
set smartcase
set incsearch
set hlsearch

" Indenting and tab
set expandtab
set shiftwidth=4
set smarttab
set tabstop=4
set wildmenu

" Lines
set cursorline
set colorcolumn=80
set textwidth=80

" Status line
set laststatus=2

" Backup files
set nobackup
set nowb

" Mouse
set mouse=a

" Clipboard
set clipboard=
vnoremap <C-y> "+y
inoremap <C-p> <C-r>+

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Maps
""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" For info on the different modes:
" :h map-modes
" :h keycodes

" Space key as leader
nmap <Space> <Nop>
let mapleader=" "

" Spacemacs-like shortcuts
nmap <Leader>iF i<C-r>=expand("%")<CR>
nmap <Leader>if i<C-r>=expand("%:t")<CR>
nmap <Leader>, <Leader>bb
nmap <Leader>bb :buffers<CR>:buffer<Space>
omap <Leader>bp :bprevious<CR>
nmap <Leader>bn :bnext<CR>
nmap <Leader>bd :bprevious<Bar>split<Bar>bnext<Bar>bdel<CR>
nmap <Leader>bN :enew<CR>
nmap <Leader><Tab> <C-^>
nmap <Leader>cw :%s/\s\+$//e<CR>
nmap <Leader>fp :e $MYVIMRC<CR>
nmap <Leader>fP :e <C-r>=expand(g:main_settings_file)<CR><CR>
nmap <Leader>ff :Explore<CR>
nmap <Leader>fs :write<CR>
nmap <Leader>fS :execute 'saveas' expand('%:h') . "/" . ""<Left>
nmap <Leader>fY :let @"=expand("%")<CR>
nmap <Leader>fy :let @"=expand("%:p")<CR>
nmap <C-s> :write<CR>
nmap <Esc>⌘s :write<CR>
imap <Esc>⌘s <Esc>:write<CR>
nmap <Leader>S :%s//g<Left><Left>
vmap <Leader>S :s//g<Left><Left>
nmap <Leader>w+ <C-w>H
nmap <Leader>W+ <C-w>J
nmap <Leader>w= <C-w>=
nmap <Leader>wh <C-w>h
nmap <Leader>wj <C-w>j
nmap <Leader>wk <C-w>k
nmap <Leader>wl <C-w>l
nmap <Leader>wr <C-w>r
nmap <Leader>ww <C-w><C-w>
nmap <Leader>w- :split<CR>
nmap <Leader>w/ :vsplit<CR>
nmap <Leader>w\ :vsplit<CR>
nmap <Leader>wd :q<CR>
nmap <Leader>qq :q<CR>
nmap <Leader>/ :nohlsearch<CR>
nmap <BS> :nohlsearch<CR>h
nmap <F6> :set spell<CR>
cnoremap w!! execute 'silent! write !SUDO_ASKPASS=`which askpass` sudo tee % >/dev/null' <bar> edit!

" Go back to Normal mode.
imap jk <Esc>

" Make some shortcuts effective to end of line.
map vC v$h
map Y y$
map H ^
map L $

" Visual mode blockwise indent
nmap > >>
nmap < <<
vmap > >gv
vmap < <gv

" Exit terminal-mode
tnoremap <Esc> <C-\><C-n>

" Autoappend closing parentheses
imap (      ()<Left>
imap (<CR>  (<CR>)<Esc>O
imap ((     (
imap ()     ()
imap [      []<Left>
imap [<CR>  [<CR>]<Esc>O
imap [[     [
imap []     []

" Move lines up and down (Shift-Alt)
nmap <S-A-j> :m .+1<CR>
nmap <S-A-k> :m .-2<CR>

" Tab navigation
nmap <tab> :tabnext<CR>
nmap <s-tab> :tabprev<CR>
nmap <C-t> :tabnew<CR>
imap <C-t> <Esc>:tabnew<CR>

" map j gj
" map k gk

" Resize window
nmap <silent> <A-=> <C-w>=
nmap <silent> <A-k> :exe "resize " . (winheight(0) + 1)<CR>
nmap <silent> <A-j> :exe "resize " . (winheight(0) - 1)<CR>
nmap <silent> <A-l> :exe "vertical resize " . string(winwidth(0) * 1.05)<CR>
nmap <silent> <A-h> :exe "vertical resize " . string(winwidth(0) * 0.95)<CR>

""""" Plugins """""
if filereadable(expand('~/.vim/plugins.vim'))
  call plug#begin('~/.vim/plugged')
  source ~/.vim/plugins.vim
  call plug#end()
endif

for g:fpath in split(globpath('~/.vim/plugins', '*.vim'), '\n')
  exe 'source' g:fpath
endfor

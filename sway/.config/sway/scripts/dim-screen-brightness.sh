#!/usr/bin/env bash
#
# Smoothly dim the screen brightness, with the option to restore it after
# changing it's value.

set -euo pipefail

QUIET=""

idle_backlight=10
fade_step_time=0.07
cache_file="/tmp/backlight.cache"

save_brightness() {
    get_brightness > "$cache_file"
}

restore_brightness() {
    val=$(cat "$cache_file")
    set_brightness "$val"
}

get_brightness() {
    float=$(light -G)
    printf '%.*f\n' 0 $float
}

set_brightness() {
    local value=$1
    for level in $(eval echo {$(get_brightness)..$value}); do
        light -S $level
        sleep $fade_step_time
    done
}

info() {
    if [ -z "$QUIET" ] ; then
        echo -e $@
    fi
}

help() {
    info "Not implemented, take a look at the source."
    exit 0
}

# while getopts "dgrsh0" opt; do
while [ $# -gt 0 ]; do
    case "$1" in
        0)
            set_brightness 0
            shift
            ;;
        d)
            set_brightness "$idle_backlight"
            shift
            ;;
        g)
            get_brightness
            exit
            shift
            ;;
        r)
            restore_brightness
            shift
            ;;
        s)
            save_brightness
            shift
            ;;
        h|-h|--help)
            help
            exit 0
            ;;
        *)
            info "Invalid argument: $1" >&2
            exit 1
            ;;
    esac
done

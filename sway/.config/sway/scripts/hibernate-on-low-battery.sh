#!/usr/bin/env bash

suspend_message="Critical battery threshold:"
battery_level_low=7

notify() {
    msg_head="$1"
    msg_body="Suspending"
    value=$2

    notify-send -u critical "$msg_head ${value}%" "$msg_body"
    sleep 5
    echo "$msg_head ${value}%" "$msg_body"
    logger "$msg_head ${value}% - $msg_body"
}

hibernate_if_low() {
    upower -i "$(upower -e | grep bat)" | grep -E 'state|percentage' \
    | awk -F'[,:%]' '{print $2, $3}' | xargs | {
        read -r state capacity
        # echo $state $capacity

        if [ "$state" = discharging -a "$capacity" -le $battery_level_low ]; then
            notify "$suspend_message" "$capacity"
            systemctl suspend
        fi
    }
}

while true; do
    hibernate_if_low
    sleep 5
done

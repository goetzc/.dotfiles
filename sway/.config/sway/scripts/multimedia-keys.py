#!/usr/bin/env python3
#
# GNU GENERAL PUBLIC LICENSE Version 3
# Copyright (C) Götz
#
"""
Application to manage multimedia keys notifications/feedback.
"""

import argparse
import logging as log
import os
import shlex
import subprocess
import sys
from pathlib import Path
from typing import List, Union

Message = Union[str, int, float]

pipe = os.environ["SWAYSOCK"] + ".wob"


def manage_volume(value: str) -> None:
    if value == "toggle":
        process_open(shlex.split("pactl set-sink-mute @DEFAULT_SINK@ toggle"))
        osd("speaker", "Volume", "%.f%%" % get_volume_mute())
    else:
        process_open(
            shlex.split("pactl set-sink-volume @DEFAULT_SINK@ " + value))
        osd("speaker", "Volume", "%.f%%" % get_volume())


def manage_display(value: str) -> None:
    process_open(shlex.split("brightnessctl set " + value))
    osd("brightness", "Brightness", "%s" % get_brightness())


def get_volume() -> float:
    output = process_check_output(["pulsemixer", "--get-volume"])
    volumes = output.decode().strip().split()
    volume = sum(map(int, volumes)) / len(volumes)
    log.debug("Volume: " + str(volume))
    return volume


def get_volume_mute() -> float:
    output = process_check_output(["pulsemixer", "--get-mute"])
    value = output.decode().strip()
    if value == "1":  # is muted
        volume = 0
    else:
        volume = get_volume()
    log.debug("Mute volume: " + str(volume))
    return volume


def get_brightness() -> str:
    output = process_check_output(["brightnessctl", "-m", "info"])
    brightness_info = output.decode().strip().split(",")
    brightness = [e for e in brightness_info if "%" in e][0]
    log.debug("Brightness: " + brightness)
    return brightness


def process_check_output(args: List[str]):
    try:
        return subprocess.check_output(args)
    except subprocess.CalledProcessError:
        error("Error adjusting value.")
        sys.exit(1)


def process_open(args: List[str]):
    try:
        return subprocess.Popen(args)
    except subprocess.CalledProcessError:
        error("Error adjusting value.")
        sys.exit(1)


def osd(icon, title, value) -> None:
    if wayland():
        wob(pipe, value)
    else:
        notify(icon, title, value)


def wob(pipe: str, value: Union[int, float]):
    wob_setup(pipe)

    with open(pipe, 'w') as f:
        print(value.strip('%'), file=f)


def wob_setup(pipe):
    if not Path(pipe).is_fifo():
        os.mkfifo(pipe)


def error(msg: str) -> None:
    notify("error", msg, timeout=5000)


def notify(icon: str, title: Message, body: Message = "",
           timeout: int = 1250) -> None:
    """Send a desktop notification"""
    subprocess.Popen([
        "notify-send",
        "--icon=%s" % icon,
        "--expire-time=%i" % timeout,
        str(title),
        str(body)
    ])


def wayland():
    if os.environ['XDG_SESSION_TYPE'] == "wayland":
        return True
    else:
        return False


def parse_args():
    parser = argparse.ArgumentParser(
        # description="Application to manage multimedia keys actions.")
        description=__doc__)
    parser.add_argument("-v",
                        "--verbose",
                        action="count",
                        default=0,
                        help="increase output verbosity")
    parser.add_argument("--volume", help="Manage audio volume", type=str)
    parser.add_argument("--display", help="Manage display", type=str)
    return parser.parse_args(None if sys.argv[1:] else ['-h'])


def main():
    args = parse_args()

    # https://docs.python.org/3/library/logging.html#levels
    if args.verbose:
        print("Verbosity turned on.")
        if args.verbose == 1:
            log.basicConfig(level=log.INFO)
        elif args.verbose >= 2:
            log.basicConfig(level=log.DEBUG)

    try:
        log.debug(args)
        log.debug("------")

        if args.volume:
            manage_volume(args.volume)
        if args.display:
            manage_display(args.display)
    except KeyboardInterrupt:
        sys.exit(0)


if __name__ == "__main__":
    main()

#!/usr/bin/env sh

wallpaper="$(find $HOME/Pictures/Wallpapers/ -type f | sort -R | tail -n 1)"

swaylock \
    --daemonize \
    --image "$wallpaper" \
    --indicator-caps-lock \
    --show-failed-attempts \
    --show-keyboard-layout
